import * as bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import * as http from 'http';
import { AuthRoutes } from './routes/auth';
import { CONFIG } from './config';

class Server {
  public app: express.Application;

  private server: http.Server;
  private authRoutes!: AuthRoutes;

  // tslint:disable-next-line:member-ordering
  public static bootstrap(): Server {
    return new Server();
  }

  constructor() {
    this.app = express();
    this.server = http.createServer(this.app);
    this.app.use(cors());
    this.app.use(bodyParser.json());

    this.attachEndpoints();
    this.listen();
  }

  private attachEndpoints() {
    this.authRoutes = new AuthRoutes();
    this.app.use('/auth', this.authRoutes.getRoutes());
  }

  private listen(): void {
    this.server.listen(CONFIG.PORT, () => {
      console.log(`Running on port: ${CONFIG.PORT}`);
    });
  }

}

export = Server.bootstrap().app;
