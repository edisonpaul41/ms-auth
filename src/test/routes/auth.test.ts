import * as chai from 'chai';
import chaiHttp = require('chai-http');
import app = require('../../app');
// tslint:disable-next-line:ordered-imports
import { request } from 'chai';

chai.use(chaiHttp);

describe('Auth Route', () => {

    it('/POST should return response on call of 200', () => {
        const requestMockup = {
            key: '2f5ae96c-b558-4c7b-a590-a501ae1c3f6c',
        };
        return request(app).post('/auth/verify')
            .send(requestMockup)
            .then((res) => {
                chai.expect(res.status).to.eql(200);
            });
    });
});
