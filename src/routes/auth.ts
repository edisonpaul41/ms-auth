import * as express from "express";
export class AuthRoutes {
  private routes = express.Router();

  public getRoutes() {
    this.initRoutes();
    return this.routes;
  }

  private initRoutes() {
    this.routes.post('/verify', (req: express.Request, res: express.Response) => {
      if (req.body.key && req.body.key === '2f5ae96c-b558-4c7b-a590-a501ae1c3f6c') {
        return res.status(200).send();
      }
      return res.status(401).send();
    });
  }
}
